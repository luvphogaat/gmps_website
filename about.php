<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GMPS</title>
    <link rel="icon" href="img/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all"/>
  </head>
<body>
<div class="container-fluid" id="bgimg1" style="//border:double;padding:0px;">
    <!--Menu Part-->
    <nav class="navbar">
  <div class="container-fluid" style="padding:0px">
    <div class="navbar-header" style="padding:0px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.php" style="display: inline-block;//border:double;padding:0px 20px;">
      <h3 style="color:#fff;">GMPS</h3>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right menu">
        <li><a href="index.php">HOME</a></li>
        <li class="active"><a href="about.php">ABOUT US</a></li>
        <li><a href="admission">ADMISSION</a></li>
        <li><a href="career">CAREER</a></li>
        <li><a href="result">RESULT</a></li>
        <li><a href="gallery.php">GALLERY</a></li>
        <li><a href="contact.php">CONTACT US</a></li>
        <li><a href="login">STAFF LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>

</div>
    <!--menu Ends Here--->
 <div class="container">
     <h3 class="text-center">ABOUT US</h3>
     <div class="row">
         <div class="col-md-12">
             <div class="text-center">
                 <img src="img/logo.png"/>
             </div>
             <div class="text-center">
                 <h3 style="color:#134074"><span class="bold2">G</span>anga <span class="bold2">M</span>emorial <span class="bold2">P</span>ublic <span class="bold2">S</span>chool</h3>
             </div>
             <br><br>
             <div class="container-fluid">
                 <p class="text-justify">
                     Ganga Memorial Public School was established in 2007. It is a progressive, english medium, co-educational school, based on Indian thoughts , culture and traditions. The School has a picturesque campus centrally at South East Delhi, a high profile residential and commercial hub of the Capital of India. The school has futuristic infrastructure, state of the art buildings, spacious well ventilated class rooms, smart classes equipped with computer aided learning..

                 </p>
             </div>
             <div class="container-fluid">
                 <h4>Principal's Desk</h4>
                 <p class="text-justify">
                     11 years of amazing teachers, amazing students, amazing growth, amazing activities and an amazing parent community support !
Without this co-operation and support, we would never be able to proceed on the important work we are entrusted with - of imparting education. Education that provides a dynamic learning environment and focuses on high quality teaching and learning. Education that imparts knowledge and develops thinking and working skills. Education that plays an important role in the building of character, instills the need to make a positive contribution to society, and helps to foster positive relationships in the environment that sustains them.
I have to say that I’m extremely impressed by the energy and hard work that the staff puts in to make sure that all the students of this school flourish and succeed.I congratulate them and thank them for their dedication and selfless commitment to their work.
Let us resolve to continue to work together to nurture, sustain and affirm the spirit and ethos of this great institution, and take it to the heights that destiny intended it to be.

                 </p>
             </div>
             <div class="container-fluid">
                 <h4>Mission &amp; Vision</h4>
                 <h5>Educate, Empower Enlighten</h5>
                 <p class="text-justify">
                     Ganga Memorial mission is to form integrated women who are capable of responding to the challenges of life with joy and a deep faith in God, whose choices and decisions are guided by right values, enriched by our cultural heritage and committed to the service of our people
                 </p>
                    <h5>We aim to:-</h5>
                     <ul>
                         <li>To live in God’s presence</li>
                         <li>Develop habit of prayer, meditation and contemplation.</li>
                         <li>To reach out to the marginalized through community service</li>
                         <li>To work towards adopting new villages to improve the lives of the inhabitants</li>
                         <li>Strive to draw out the full potential of our students</li>
                         <li>To develop and deliver quality education to all students</li>
                         <li>To address the aspirations and abilities of all students</li>
                         <li>To nurture innovation, creativity and technological abilities of all students</li>
                         <li>Nurture and harness leadership ability among all students</li>
                         <li>Develop world class students</li>
                         <li>Encourage active participation in co and extra-curricular activities.</li>
                     </ul>
                 
             </div>
             <br><br>
         </div>
     </div>
 </div>
    <!--footer starts-->
  <?php include('footer.php') ?>
</body>
</html>