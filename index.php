<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GMPS</title>
    <link rel="icon" href="img/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all"/>
    <style>
        #myCarousel{
/*         position: absolute;*/
/*            border:double;*/
        }
        .carousel-inner > .item > img{
            height: 600px !important;
        }
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
/*      position: relative;*/
      margin:auto;
  }
  </style>
<!--    <link rel="preload" href="css/style.css" as=""/>-->
</head>
<body>
<div class="container-fluid" id="bgimg" style="//border:double;padding:0px;">
   <!--Menu Part-->
    <nav class="navbar" style="//border:double;margin-bottom:0px;">
  <div class="container-fluid" style="padding:0px">
    <div class="navbar-header" style="//padding:0px;margin-right:0px;margin-left:0px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index" style="display: inline-block;//border:double;padding:0px 20px;">
      <h3 style="color:#fff;">GMPS</h3>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right menu">
        <li class="active"><a href="index">HOME</a></li>
        <li><a href="about">ABOUT US</a></li>
        <li><a href="admission">ADMISSION</a></li>
        <li><a href="career">CAREER</a></li>
        <li><a href="result">RESULT</a></li>
        <li><a href="gallery">GALLERY</a></li>
        <li><a href="contact">CONTACT US</a></li>
        <li><a href="login">STAFF LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>
   <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <div class="item active">
        <img src="img/slider/slide1.jpg" alt="Chania" width="460" height="345">
        
      </div>

      <div class="item">
        <img src="img/slider/slide2.jpg" alt="Chania" width="460" height="345">
      </div>
    
      <div class="item">
        <img src="img/slider/slide3.jpg" alt="Flower" width="460" height="345">
      </div>

      <div class="item">
        <img src="img/slider/slide4.jpg" alt="Flower" width="460" height="345">
      </div>
  
    </div>

    <!-- Left and right controls -->
<!--
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
-->
  </div>

    </div>
    <div class="container" style="//border:double;padding:20px;background-color:#fff;">
        <h3>Welcome to Ganga Memorial Public School</h3>
        <hr>
        <div class="row" style="padding-right:20px">
            <div class="col-md-4 text-center">
                <img src="img/logo.png" width="150"/>
            </div>
            <div class="col-md-5" style="padding-top:20px;text-align:justify">
                <p>Ganga Memorial Public School was established in 2007. It is a progressive, english medium, co-educational school, based on Indian thoughts , culture and traditions. The School has a picturesque campus centrally at South East Delhi, a high profile residential........<br><br>
                <a class="btn btn-default" href="about">Read More</a>
</p>
            </div>
            <div class="col-md-2 col-md-offset-1" style="border:1px solid">
               <h5>News &amp; Events</h5>
               <br>
               <small>No News or Event</small>
                <marquee behavior="scroll" direction="up" onmouseover="this.stop();" onmouseout="this.start();" style="//border:double;height:100px">
                    
                  </marquee>
            </div>
        </div>
    </div>
    <div class="container" style="//border:double;padding:20px;background-color:#fff;">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default shortnote">
                    <div class="panel-heading">
                    <img src="img/Principal_Icon.jpg" width="100%" height="270px" style="border:none !important"/></div>
                    <div class="panel-body">
                    <h4>Principal 's Desk</h4>
                    <p style="text-align:justify">11 years of amazing teachers, amazing students, amazing growth, amazing activities and an amazing parent community support !
                        Without this co-operation and support, we would never be able to.............</p>
                    <br>
                        <a class="btn btn-default" href="about">Read More</a>
                    </div>
                  </div>
            </div>
            <div class="col-md-4">
                 <div class="panel panel-default shortnote">
                    <div class="panel-heading"><img src="img/missionvision.jpg" width="100%" height="270px" /></div>
                    <div class="panel-body">
                    <h4>Mission &amp; Vision</h4>
                    <p style="text-align:justify">Ganga Memorial mission is to form integrated women who are capable of responding to the challenges of life with joy and a deep faith in God, whose choices and decisions are guided by right values.............</p>
                    <br>
                        <a class="btn btn-default" href="about">Read More</a>
                    </div>
                  </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default shortnote">
                    <div class="panel-heading"><img src="img/admission.jpg" width="100%" height="270px"/></div>
                    <div class="panel-body">
                    <h4>Admissions</h4>
                    <p style="text-align:justify">
                        Click on Read More to know more button to get details about the Admissions for the session 2018-2019
                    </p>
                    <br>
                    <br>
                    <br>
                    
                        <a class="btn btn-default" href="admission">Read More</a>
                    </div>
                  </div>
            </div>
        </div>
        <div class="row">
           <h3 style="padding-left:20px;"><span>G</span>allery</h3>
            <div class="col-md-3">
                <img src="img/gallery0.JPG" style="width: 100%" class="img-rounded"/>
            </div>
            <div class="col-md-3">
                <img src="img/gallery1.JPG" style="width: 100%" class="img-rounded"/>
            </div>
            <div class="col-md-3">
                <img src="img/gallery2.JPG" style="width: 100%" class="img-rounded"/>
            </div>
            <div class="col-md-3">
                <img src="img/gallery3.JPG" style="width: 100%" class="img-rounded"/>
            </div>
            <br>
                <a class="btn btn-default" href="gallery" style="margin-left: 14px;margin-top:20px;">See More</a>
        </div>
    </div>
   
<?php
    include('footer.php');    
?>

    </body>
</html>