<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GMPS</title>
    <link rel="icon" href="img/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all"/>
</head>
<body>
<div class="container-fluid" id="bgimg1" style="//border:double;padding:0px;//min-height:350px;">
    <!--Menu Part-->
    <nav class="navbar">
  <div class="container-fluid" style="padding:0px">
    <div class="navbar-header" style="padding:0px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.php" style="display: inline-block;//border:double;padding:0px 20px;">
      <h3 style="color:#fff;">GMPS</h3>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right menu">
        <li><a href="index.php">HOME</a></li>
        <li><a href="about.php">ABOUT US</a></li>
        <li class="active"><a href="admission">ADMISSION</a></li>
        <li><a href="career">CAREER</a></li>
        <li><a href="result">RESULT</a></li>
        <li><a href="gallery.php">GALLERY</a></li>
        <li><a href="contact.php">CONTACT US</a></li>
        <li><a href="login">STAFF LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>    
</div>
    <!--menu Ends Here--->
         <div class="container" style="padding:30px 0px">
             <h3 class="text-center">ADMISSION</h3>
             <div class="container-fluid">
                <div class="row">
                    <div class="col-md-9">
                <p  style="text-align:justify">
                    Offline registrations for admission to the Academic session 2018-19.
                </p>
                <p  style="text-align:justify">
                    Thank you for showing interest in your own school. The Admission Team at The Ganga Memorial Public School will help you with the admissions process and all related formalities. We look forward to welcoming you and your family to our warm, well knit, and happy educational group.
                </p>
                <p style="text-align:justify">
                    We make every effort and have procedures in place to identify the needs of all prospective students prior to admission, so they may access and benefit from the curriculum on offer. 
                </p>
                <p>
                    Please get in touch with the Admissions team. 
                </p>
                <p>
                    <a href="resources/ADMISSION%20FORM.pdf" download>Click here</a> to download the admission form. 
                </p>
                <div>
                    Do remember
                    <ul>
                        <li style="text-align:justify">Applicants from all backgrounds, nationalities and education systems will be considered.</li>
                        <li style="text-align:justify">An acceptance of admission at The Ganga Memorial Public School signifies that you accept the school policies and agree to abide by those.</li>
                    </ul>
                </div>
                <p>
                    Disclaimer clause on Admissions
                </p>
                <p>
                    All prospective parents should understand that:
                </p>
                <p style="text-align:justify">
                    The Ganga Memorial Public School admissions process is strictly managed in order to be fair, transparent and in line with the values. 
                </p>
                <p style="text-align:justify">
                    No payment of any type, except those set out in the fees schedule, will be requested by anyone associated with The Ganga Memorial Public School.
                </p>
                <div class="status">
                    <h3 style="color:#F56960">Admission Now Closed</h3>
                </div>
                    </div>
                    <div class="col-md-3">
                        <img src="img/admission_closed.PNG" class="img-responsive img-rounded" width="300"/>
                    </div>
                </div>
             </div>
         </div>
    <!--footer starts-->
  <?php include('footer.php') ?>
</body>
</html>