<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GMPS</title>
    <link rel="icon" href="img/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all"/>
</head>
<body>
<div class="container-fluid" id="bgimg1" style="//border:double;padding:0px;//min-height:350px;">
    <!--Menu Part-->
    <nav class="navbar">
  <div class="container-fluid" style="">
    <div class="navbar-header" style="padding:0px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.php" style="display: inline-block;//border:double;padding:0px 20px;">
      <h3 style="color:#fff;">GMPS</h3>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right menu">
        <li><a href="index.php">HOME</a></li>
        <li><a href="about.php">ABOUT US</a></li>
        <li><a href="admission">ADMISSION</a></li>
        <li class="active"><a href="career">CAREER</a></li>
        <li><a href="result">RESULT</a></li>
        <li><a href="gallery.php">GALLERY</a></li>
        <li><a href="contact.php">CONTACT US</a></li>
        <li><a href="login">STAFF LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>

</div>
    <!--menu Ends Here--->
         <div class="container" style="padding:30px 0px">
             <h3 class="text-center">CAREER</h3>
             <div class="container-fluid">
                <p>
                    The Ganga Memorial Public  School, New Delhi is a multicultural school delivering outstanding education to students from ages 2 to 10. The school combines the best of  education and draws inspiration from Indian culture and tradition.</p>
                    <br>
                    <p>We aim to recruit and retain the best possible staff. The Human Resources department is committed to providing strategic and operational support to the school to accomplish this goal. The Ganga Memorial Public School recruits its staff through external advertisements and referrals.</p>
                    <br>
                    <p>The Ganga Memorial Public School offers a very competitive compensation and benefits structure. Salary will be commensurate with experience and qualifications. </p>
                    <p>Please mail your resume at career@gangaschool.in
                </p>
             </div>
         </div>
    <!--footer starts-->
  <?php include('footer.php') ?>
</body>
</html>