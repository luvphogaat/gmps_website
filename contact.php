<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GMPS</title>
    <link rel="icon" href="img/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all"/>
</head>
<body>
<div class="container-fluid" id="bgimg1" style="//border:double;padding:0px;">
    <!--Menu Part-->
    <nav class="navbar">
  <div class="container-fluid" style="padding:0px">
    <div class="navbar-header" style="padding:0px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index" style="display: inline-block;//border:double;padding:0px 20px;">
      <h3 style="color:#fff;">GMPS</h3>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right menu">
        <li><a href="index">HOME</a></li>
        <li><a href="about">ABOUT US</a></li>
        <li><a href="admission">ADMISSION</a></li>
        <li><a href="career">CAREER</a></li>
        <li><a href="result">RESULT</a></li>
        <li><a href="gallery">GALLERY</a></li>
        <li class="active"><a href="contact">CONTACT US</a></li>
        <li><a href="login">STAFF LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>
</div>
    <!--menu Ends Here--->
   <div class="container">
      <h3 class="text-center">Contact Us</h3>
      <br>
       <div class="row">
           <div class="col-md-7">
              
               <form method="post" action="contactus.php">
                   <div class="form-group">
                       <input type="text" name="uname" placeholder="Your Name" class="form-control" />
                   </div>
                   <div class="form-group">
                       <input type="text" name="email" placeholder="Your Email ID"  class="form-control"/>
                   </div>
                   <div class="form-group">
                       <input type="text" name="mob" placeholder="Your Contact Number" class="form-control" />
                   </div>
                   <div class="form-group">
                      <textarea name="message" rows="10" class="form-control" placeholder="Your Message"></textarea>
                   </div>
                   <div class="form-group">
                      <button class="btn btn-default">SEND MESSAGE</button>
                      <button class="btn btn-default pull-right" type="reset">RESET</button>
                   </div>
               </form>
           </div>
           
           <div class="col-md-5">
              <div class="row">
                   <div class="col-md-12">
                       <!--Map Area-->
                       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d876.4836036492682!2d77.26548182917197!3d28.51162089890409!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce13fa17e7e25%3A0xe5f89047af69a3!2sGanga+Memorial+Public+School!5e0!3m2!1sen!2sin!4v1520769988594" width="400" height="220" frameborder="0" style="border:0" allowfullscreen></iframe>
                   </div>
               </div>
               <br>
               <div class="row">
                   <div class="col-md-12">
                       <!--Address Area-->
                       <h4>Address</h4>
                       <p><i class="fa fa-map-marker" aria-hidden="true"></i> 19, Jalam Mohalla , Tughlakabad Village , New Delhi 110044</p>
                       <p><i class="fa fa-map-marker" aria-hidden="true"></i> 48/2, Tughlakabad Extension , New Delhi - 110019</p>
                       <p><i class="fa fa-phone" aria-hidden="true"></i> Contact Number : 011-29991716</p>
                       <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Email ID: gangaschool2007@gmail.com</p>
                   </div>
               </div>
           </div>
       </div>
   </div>
   <br><br>
    <!--footer starts-->
<?php include('footer.php') ?>
</body>
</html>