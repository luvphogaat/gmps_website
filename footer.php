<!---Footer--->
  <div style="">
       <div class="container-fluid" style="height:auto;background-color:#134074;color:#fff;padding-top:30px;//padding-bottom:10px;">
       <div class="container">
        <footer>
           <div class="row" style="padding-bottom:50px;">
               <div class="col-md-3 text-left" style="//border:double;padding-top:20px;">
                   <img src="img/logo12.PNG" width="100"/>
                   <h5><span class="bold1">G</span>anga <span class="bold1">M</span>emorial <span class="bold1">P</span>ublic <span class="bold1">S</span>chool</h5>
               </div>
               <div class="col-md-7" id="quicklinks">
                   <h5>Quick Links</h5>
                   <ul>
                       <li><a href="index">Home</a></li>
                       <li><a href="about">About US</a></li>
                       <li><a href="admission">Admission</a></li>
                       <li><a href="career">Career</a></li>
                       <li><a href="result">Result</a></li>
                       <li><a href="gallery">Gallery</a></li>
                       <li><a href="contact">Contact Us</a></li>
                       <li><a href="#">Staff Login</a></li>
                   </ul>
               </div>
               <div class="col-md-2" id="followus">
                   <h5>Connect with Us</h5>
                   <br>
                   <span style="border:2px solid;padding:8px 10px;" class="round"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                   <span style="border:2px solid;padding:8px 12px;" class="round"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                   <span style="border:2px solid;padding:8px 9px;" class="round"><i class="fa fa-google-plus" aria-hidden="true"></i></span>
               </div>
           </div>
           
        </footer>
       </div>
   </div>
    <div class="text-center" style="background-color:#133074;color:#fff;padding:15px 0px">
                Copyright &copy; <?php echo date('Y')?> Ganga Memorial Public School, All Rights Reserved
    </div>
</div>
<script src="css/googleapis.min.js"></script>
<script src="css/bootstrap.min.js"></script>
