<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GMPS</title>
    <link rel="icon" href="img/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all"/>
<!--    <link href="css/simplelightbox.min.css" type="text/css" rel="stylesheet"/>-->
</head>
<body>
<div class="container-fluid" id="bgimg1" style="//border:double;padding:0px;">
    <!--Menu Part-->
    <nav class="navbar">
  <div class="container-fluid" style="padding:0px">
    <div class="navbar-header" style="padding:0px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index" style="display: inline-block;//border:double;padding:0px 20px;">
      <h3 style="color:#fff;">GMPS</h3>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right menu">
        <li><a href="index">HOME</a></li>
        <li><a href="about">ABOUT US</a></li>
        <li><a href="admission">ADMISSION</a></li>
        <li><a href="career">CAREER</a></li>
        <li><a href="result">RESULT</a></li>
        <li class="active"><a href="gallery">GALLERY</a></li>
        <li><a href="contact">CONTACT US</a></li>
        <li><a href="login">STAFF LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>

</div>
    <!--menu Ends Here--->
    <div style="width:100%;height:auto !important;//border:double;background:#fff;">
  	<div class="container container1" style="//border:double;">
		<h1 class="align-center" style="font-size:50px;">GALLERY</h1>
		<div class="gallery">
			
			    <img src="img/gallery0.JPG" alt="" title="" style="height:300px !important" class="big"/>
            
			    <img src="img/gallery1.JPG" alt="" title="" style="height:300px !important"/>
            
                <img src="img/gallery2.JPG" alt="" title="" style="height:300px !important"/>
            
                <img src="img/gallery3.JPG" alt="" title="" style="height:300px !important"/>
            
			<div class="clear"></div>

		</div>
		<br><br>
			
	</div>

</div>

    <!--footer starts-->
  <?php include('footer.php'); ?>
</body>
</html>