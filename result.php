<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GMPS</title>
    <link rel="icon" href="img/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all"/>
</head>
<body>
<div class="container-fluid" id="bgimg1" style="//border:double;padding:0px;">
    <!--Menu Part-->
    <nav class="navbar">
  <div class="container-fluid" style="padding:0px">
    <div class="navbar-header" style="padding:0px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.php" style="display: inline-block;//border:double;padding:0px 20px;">
      <h3 style="color:#fff;">GMPS</h3>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right menu">
        <li><a href="index.php">HOME</a></li>
        <li><a href="about.php">ABOUT US</a></li>
        <li><a href="admission">ADMISSION</a></li>
        <li><a href="career">CAREER</a></li>
        <li class="active"><a href="result">RESULT</a></li>
        <li><a href="gallery.php">GALLERY</a></li>
        <li><a href="contact.php">CONTACT US</a></li>
        <li><a href="login">STAFF LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>

</div>
    <!--menu Ends Here--->
 <div class="container" style="padding-bottom:30px;height:288px;">
     <h3 class="text-center">RESULT PAGE</h3>
     <div class="container-fluid">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered" cellpadding="20">
                <thead style="background:#134074;color:#fff;">
                    <tr>
                        <th style="padding-left:20px">Title</th>
                        <th width="20%" style="padding-left:20px">Date</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
<!--                    <td></td><td></td>-->
                </tr>
                </tbody>
            </table>
        </div> 
     </div>
     
    </div>
    <!--footer starts-->
  <?php include('footer.php') ?>
</body>
</html>