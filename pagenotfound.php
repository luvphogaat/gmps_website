<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GMPS</title>
    <link rel="icon" href="img/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Aldrich" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Nosifer" rel="stylesheet">    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<div class="container-fluid" id="bgimg1" style="//border:double;padding:0px;">
    <!--Menu Part-->
    <nav class="navbar">
  <div class="container-fluid" style="padding:0px">
    <div class="navbar-header" style="padding:0px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index" style="display: inline-block;//border:double;padding:0px 20px;">
      <h3 style="color:#fff;">GMPS</h3>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right menu">
        <li><a href="index">HOME</a></li>
        <li><a href="about">ABOUT US</a></li>
        <li><a href="admission">ADMISSION</a></li>
        <li><a href="career">CAREER</a></li>
        <li><a href="result">RESULT</a></li>
        <li><a href="gallery">GALLERY</a></li>
        <li><a href="contact">CONTACT US</a></li>
        <li><a href="login">STAFF LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>
</div>
    <!--menu Ends Here--->
   <div class="container">
        <div style="//border:double;//height:598px;">
    <div style="width:70%;float:left;//border:double;text-align:center;margin-top:50px;font-family: 'Nosifer', cursive;">
        <h5 style="font-family: 'Nosifer', cursive;color:#FF8552">Don't worry you will be back on track in no time!</h5>
        <h1><span style="font-size:250px;font-weight:bolder;font-family: 'Nosifer', cursive;;color:#FB6107">404</span><small style="font-family: 'Nosifer', cursive;"> Error</small></h1>
        <p style="color:#FF8552">Page doesn't exist or some other error occurred.</p>
        <p style="color:#FF8552">Go back to Home Page.. <a href="index" style="text-decoration:none">Click Here</a></p>
    </div>
    <div style="float:left;//border:double;margin-top:200px;margin-left:100px">
        <img src='img/lost.png'/>
    </div>    
</div>
  
   </div>
   <br><br>
    <!--footer starts-->
<?php include('footer.php') ?>
</body>
</html>